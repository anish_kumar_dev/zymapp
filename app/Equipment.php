<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * Description of Equipment
 *
 * @author anish
 */
class Equipment extends Model {
    //put your code here
    
    protected $id;
    protected $name;
    protected $equipDescription;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'equipDescription',
    ];
    
    
}
