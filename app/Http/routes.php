<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $app->get('/', function () use ($app) {
// 	// return 'Hello World';
	
// echo "hey!!";

//     return $app->version();
// });


// $app->get('/','UserController@index');
$app->get('user/{id}','UserController@getUser');
$app->post('user','UserController@createUser');
$app->put('user/{id}','UserController@updateUser');
$app->delete('user/{id}','UserController@deleteUser');
$app->post('user/signIn','UserController@signInUser');

$app->get('equip/{id}','EquipmentController@getEquipment');
$app->post('equip','EquipmentController@createEquipment');
$app->get('exerciseList','ExerciseController@getExerciseList');
$app->get('exercise/{id}','ExerciseController@getExercise');
$app->post('exercise','ExerciseController@createExercise');
$app->get('workout/{id}','WorkoutController@getWorkout');
$app->get('workoutList','WorkoutController@getWorkoutList');


// $app->group(['prefix' => 'api/v1','namespace' => 'App\Http\Controllers'], function($app) {

// 	$app->get('/','UserController@index');
// 	$app->get('user/{id}','UserController@getUser');

//     $app->get('book','BookController@index');
  
//     $app->get('book/{id}','BookController@getbook');
      
//     $app->post('book','BookController@createBook');
      
//     $app->put('book/{id}','BookController@updateBook');
      
//     $app->delete('book/{id}','BookController@deleteBook');
// });

