<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Laravel\Lumen\Routing\Controller as BaseController;


class Controller extends BaseController
{
    
    /**
     * Throw the failed validation exception.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     */
//    protected function throwValidationException(Request $request, $validator)
//    {
//        echo 'HEY';
//    }
    
    /**
     * {@inheritdoc}
     */
    protected function formatValidationErrors(Validator $validator)
    {
        if (isset(static::$errorFormatter)) {
            return call_user_func(static::$errorFormatter, $validator);
        }
        $response = array('status' => 0 , 'message' => $validator->messages()->first(),'result' => (object) array());
        echo json_encode($response);
        die;
//        dd($response);
//        return response()->json($response);
//        
//        return $validator->errors()->getMessages();
    }
    
    public function apiResponse($data) {
        $response = array('status' => 1 ,'result' => (object) $data);
        return response()->json($response);
    }
    public function apiResponseError($message) {
        $response = array('status' => 0 ,'message' => $message);
        return response()->json($response);
    }
}
