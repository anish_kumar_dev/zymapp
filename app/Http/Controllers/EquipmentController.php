<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\Equipment;
use App\Http\Requests\BaseRequest;


class EquipmentController extends Controller
{
    
    
    public function getEquipment($id){
  
        
        $equipment  = Equipment::find($id);
        return $this->apiResponse($equipment);
    }
    
    public function createEquipment(Request $request) {
        
        $messages = array(
                'name.required' => 'We need to know Equipment Name',
            );
        $validationRules = array(
                'name' => 'required',
            );
        $this->validate($request, $validationRules, $messages);
        $equipment = Equipment::create($request->all());
        
        return $this->apiResponse($equipment);
        
    }
    
    
}


