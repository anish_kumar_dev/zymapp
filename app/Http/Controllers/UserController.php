<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\User;
use App\Http\Requests\BaseRequest;

class UserController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {


        $users = User::all();
        return $users;
    }

    public function getUser($id){
  
        
        $user  = User::find($id);
        //$user  = User::findOrFail($id);
        return $this->apiResponse($user);
    }
    
    public function signInUser(Request $request){
  
        
        $messages = array(
                'email.required' => 'We need to know your e-mail address!',
                'password.required' => 'Please enter password.',
            );
        $validationRules = array(
                'email' => 'required',
                'password' => 'required'
            );
        
        
        $this->validate($request, $validationRules, $messages);
        $email = $request->input('email');
        $password = $request->input('password');
        $users = User::where('email', $email)->where('password', $password)->get();
        if (count($users) > 0) {
            return $this->apiResponse($users[0]);
        }
        return $this->apiResponseError("No User Found.");
    }


//    public function createUser(RegisterUserRequest $request) {
    public function createUser(Request $request) {
        
//        dd($request->input()['email']);
        
//        $name = $request->input('email');
//        
//        dd($name);
        $messages = array(
                'email.required' => 'We need to know your e-mail address!',
                'email.unique' => 'Email already registered.',
                'mobileNumber.unique' => 'Mobile Number already registered.',
            );
        $validationRules = array(
                'email' => 'required|unique:users',
                'mobileNumber' => 'unique:users'
            );
        
        $this->validate($request, $validationRules, $messages);
        $user = User::create($request->all());
        return $this->apiResponse($user);
        
    }

    public function updateUser(Request $request, $id){
 
//        $name = $request->input('email');
//        dd($name);
        $messages = array(
                'email.required' => 'We need to know your e-mail address!',
                'email.unique' => 'Email already registered.',
                'mobileNumber.unique' => 'Mobile Number already registered.',
            );
        $validationRules = array(
                'email' => 'required|unique:users',
                'mobileNumber' => 'unique:users'
            );
        
        $this->validate($request, $validationRules, $messages);
        
        $user  = User::find($id);

        
        // $user->firstName = $request->input('firstName');
        // $user->lastName = $request->input('lastName');

        $user->email = $request->input('email');
        $user->save();
 
        return response()->json($user);
    }

    public function deleteUser($id){
        $user  = User::find($id);
        $user->delete();
 
        return response()->json('Removed successfully.');
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function showProfile($id)
    {
        return ['user' => User::findOrFail($id)];
    }


}