<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\Workout;
use App\Exercise;
use App\Http\Requests\BaseRequest;
/**
 * Description of WorkoutController
 *
 * @author anish
 */
class WorkoutController extends Controller {
    //put your code here
    public function getWorkout($id){
        $workout  = Workout::find($id);
        $workoutId = $workout->id;
        $exer = \App\WorkoutExercise::where('workout_id', $id)->where('user_id', '1')->get();
        $exerId = $exer[0]->exercise_id;
        $exercise  = \App\Exercise::find($exerId);
        $workout['exercise'] = $exercise;
        return $this->apiResponse($workout);
    }
    
    public function getWorkoutList(){
        $workouts  = Workout::all();
        foreach ($workouts as $workout) {
            $workoutId = $workout->id;
            $exer = \App\WorkoutExercise::where('workout_id', $workout->id)->where('user_id', '1')->get();
            $exerId = $exer[0]->exercise_id;
            $exercise  = \App\Exercise::find($exerId);
            $workout['exercise'] = $exercise;
        }
        return $this->apiResponse($workouts);
    }
    
}
