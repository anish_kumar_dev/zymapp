<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use App\Exercise;
use App\Equipment;
use App\ExerciseEquipment;
use App\Http\Requests\BaseRequest;

/**
 * Description of ExerciseController
 *
 * @author anish
 */
class ExerciseController extends Controller {
    //put your code here
    public function getExercise($id){
//        $exercise  = Exercise::with('equipment')->find($id);
        $exercise  = Exercise::find($id);
        
        $equip = ExerciseEquipment::where('exercise_id', $id)->where('user_id', '1')->get();
        $equipId = $equip[0]->equip_id;
        $equipment  = Equipment::find($equipId);
        $exercise['equipment'] = $equipment;
//        return $this->apiResponse($equipment);
        return $this->apiResponse($exercise);
    }
    
    public function getExerciseList(){
        $exercises  = Exercise::all();
        foreach ($exercises as $exercise) {
            $equip = ExerciseEquipment::where('exercise_id', $exercise->id)->where('user_id', '1')->get();
        $equipId = $equip[0]->equip_id;
        $equipment  = Equipment::find($equipId);
        $exercise['equipment'] = $equipment;
        }
        return $this->apiResponse($exercises);
    }
    
    public function createExercise(Request $request) {
        
        
        $messages = array(
                'name.required' => 'We need to know Equipment Name.',
            'userId.required' => 'userId is Required.',
            'equipmentId.required' => 'equipmentId is Required.',
            );
        $validationRules = array(
                'name' => 'required',
            'userId' => 'required',
            'equipmentId' => 'required',
            );
        $this->validate($request, $validationRules, $messages);
        $equipId = $request->input('equipmentId');
        $userId = $request->input('userId');
        $equipment  = Equipment::find($equipId);
        if ($equipment) {
            $exercise = Exercise::create($request->all());
//            dd($exercise);
            if ($exercise) {
                $ex_id = $exercise->id;
                $arr = ['exercise_id' => $ex_id, 'equip_id' => $equipId, 'user_id' => $userId,];
                $exerciseEquipment = ExerciseEquipment::create($arr);
                
            }
        }  else {
                return $this->apiResponseError("No Equipment Found for 'equipmentId'.");
            }
        $exercise['equip'] = $equipment;        
        return $this->apiResponse($exercise);    
    }
    
}
