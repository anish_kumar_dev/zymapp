<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

/**
 * BaseRequest Request
 *
 * This request is used to as base class for all api request.
 *
 * @author 
 */
class BaseRequest extends Request {
    
}