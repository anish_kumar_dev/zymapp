<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use App\Models\User;
use App\Models\DeviceToken;

/**
 * RegisterUserRequest Request
 *
 * This request is used to validate all input for user registeration.
 *
 * @author Jitender <jitender.thakur@appster.in>
 */
class RegisterUserRequest extends BaseRequest {
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        echo "Validating rules";die;

        return [
            'provider' => 'required|in:' . User::USER_PROVIDER_EMAIL . ',' . User::USER_PROVIDER_FACEBOOK,
            'providerId' => 'required_if:provider,' . User::USER_PROVIDER_FACEBOOK . '|unique:social_providers,provider_id',
            'type' => 'required|in:' . User::USER_TYPE_BRAND . ',' . User::USER_TYPE_FASHIONISTA,
            'email' => 'required_if:provider,' . User::USER_PROVIDER_EMAIL . '|email|unique_with:users,email,deleted_at',
            'password' => 'required_if:provider,' . User::USER_PROVIDER_EMAIL,
            'name' => 'required|max:' . User::NAME_MAX_LENGTH,
            'gender' => 'required_if:type,' . User::USER_TYPE_FASHIONISTA . '|in:' . User::GENDER_MALE . ',' . User::GENDER_FEMALE . ',' . User::GENDER_OTHERS,
            'brandName' => 'required_if:type,' . User::USER_TYPE_BRAND . '|max:' . User::BRAND_NAME_MAX_LENGTH,
            'abn' => 'required_if:type,' . User::USER_TYPE_BRAND . '',
            'mobileNumber' => 'required_if:type,'. User::USER_TYPE_BRAND,
            'address' => 'required_if:type,' . User::USER_TYPE_BRAND . '|max:' . User::ADDRESS_MAX_LENGTH,
            'description' => 'max:' . User::ADDRESS_MAX_LENGTH,
            'storeLocation' => 'max:' . User::STORE_LOCATION_MAX_LENGTH,
            'deviceType' => 'required|in:' . DeviceToken::DEVICE_TYPE_IOS . ',' . DeviceToken::DEVICE_TYPE_ANDROID,
            'deviceToken' => 'required_if:type,'. User::USER_TYPE_FASHIONISTA
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Custom error messages of the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages() {
        return [
            'provider.in' => 'The provider field must be ' . User::USER_PROVIDER_EMAIL . ' or ' . User::USER_PROVIDER_FACEBOOK . '.',
            'type.in' => 'The type field must be ' . User::USER_TYPE_BRAND . ' or ' . User::USER_TYPE_FASHIONISTA . '.',
            'gender.in' => 'The gender field must be ' . User::GENDER_MALE . ', ' . User::GENDER_FEMALE . ' or ' . User::GENDER_OTHERS . '.',
            'deviceType.in' => 'The device type field must be ' . DeviceToken::DEVICE_TYPE_IOS . ' or ' . DeviceToken::DEVICE_TYPE_ANDROID . '.',
            'providerId.unique' => 'The facebook account has already been registerd.'
        ];
    }

}
