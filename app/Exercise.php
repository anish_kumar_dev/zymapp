<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Exercise
 *
 * @author anish
 */
class Exercise extends Model {
    //put your code here
    protected $table = 'exercises';
    protected $id;
    protected $name;
    protected $exDescription;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'exDescription',
    ];
    
    public function equipment() {
        return $this->hasOne('App\ExerciseEquipment');
    }
    
}
