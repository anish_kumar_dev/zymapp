<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of Workout
 *
 * @author anish
 */
class Workout extends Model {
    //put your code here
    
    protected $table = 'workouts';
    protected $id;
    protected $name;
    protected $workoutDescription;
}

class WorkoutExercise extends Model {
    //put your code here
    protected $table = 'workout_exercises';
    protected $exercise_id;
    protected $workout_id;
    protected $user_id;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exercise_id', '$workout_id', 'user_id',
    ];
    
}