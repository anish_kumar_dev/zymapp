<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of ExerciseEquipment
 *
 * @author anish
 */
class ExerciseEquipment extends Model {
    //put your code here
    protected $exercise_id;
    protected $equip_id;
    protected $user_id;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exercise_id', 'equip_id', 'user_id',
    ];
    
}
