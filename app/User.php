<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'lastName', 'email', 'mobileNumber', 'gender', 'joiningDate', 'dob',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    protected $id;
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $mobileNumber;
    // protected $facebookId;
    protected $password;
    protected $gender;
    protected $joiningDate;
    protected $dob;


    /**
     * This constant is used for admin user.
     */
    const USER_TYPE_ADMIN = 1;

    /**
     * This constant is used for brand user.
     */
    const USER_TYPE_BRAND = 2;

    /**
     * This constant is used for fashionista user.
     */
    const USER_TYPE_FASHIONISTA = 3;

    /**
     * This constant is used for Male gender.
     */
    const GENDER_MALE = 'Male';

    /**
     * This constant is used for Female gender.
     */
    const GENDER_FEMALE = 'Female';

    /**
     * This constant is used for Others gender.
     */
    const GENDER_OTHERS = 'Others';

    /**
     * This constant is used for max length of user name field.
     */
    const NAME_MAX_LENGTH = 100;

    /**
     * This constant is used for max length of brand name field.
     */
    const BRAND_NAME_MAX_LENGTH = 100;

    /**
     * This constant is used for max length of address field.
     */
    const ADDRESS_MAX_LENGTH = 100;

    /**
     * This constant is used for max length of store location field.
     */
    const STORE_LOCATION_MAX_LENGTH = 200;

    /**
     * This constant is used for max length of description field.
     */
    const DESCRIPTION_MAX_LENGTH = 200;

    /**
     * This constant is used to define destination folder for user image and cover image.
     */
    const IMAGE_UPLOAD = 'users/';

    /**
     * This constant is used to validate email provider type while login or register.
     */
    const USER_PROVIDER_EMAIL = 'email';

    /**
     * This constant is used to validate facebook provider type while login or register.
     */
    const USER_PROVIDER_FACEBOOK = 'facebook';

    /**
     * This constant is used to represent image type in user table while uploading and deleting image.
     */
    const USER_IMAGE = 1;

    /**
     * This constant is used to represent cover image type in user table while uploading and deleting image.
     */
    const USER_COVER_IMAGE = 2;

    /**
     * This constant is used for checking notifications settings type while updating user.
     */
    const NOTIFICATION_SETTINGS = 1;
    
    /**
     * This constant is used for checking privacy settings type while updating user.
     */
    const PRIVACY_SETTINGS = 2;

}
