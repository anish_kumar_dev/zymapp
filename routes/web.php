<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// use App\User;
use App\Http\Controllers\UserController;


// dd($app);

$app->get('user', function() use ($app) {
    return "Lumen RESTful API By CoderExample)";
});

$app->get('/', function() use ($app) {
    return "Lumen RESTful API By CoderExample (https://coderexample.com)";
});



// $app->get('user/{id}','UserController@index');

// $app->group(['prefix' => 'api/v1','namespace' => 'App\Http\Controllers'], function($app) {

// 	$app->get('user','App\Http\Controllers\UserController@index');
// 	$app->get('user/{id}','UserController@getUser');

//     $app->get('book','BookController@index');
  
//     $app->get('book/{id}','BookController@getbook');
      
//     $app->post('book','BookController@createBook');
      
//     $app->put('book/{id}','BookController@updateBook');
      
//     $app->delete('book/{id}','BookController@deleteBook');
// });
